//
//  ListController.swift
//  WaferMessengerTest
//
//  Created by Augusto Cunha Lima on 24/07/2018.
//  Copyright © 2018 Augusto Cunha Lima. All rights reserved.
//

import UIKit
    let cellIdentifier = "swipeCell"
    let navigationTitle = "Countries"
    let urlRequest = "https://restcountries.eu/rest/v2/all"

class ListController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var countries: [Country] = []
    
    override func viewDidLoad() {
        self.navigationItem.title = navigationTitle
//        I am not using classes to prepare the request because the time, like manager/busyness/provider
        requestData()
    }
}

extension ListController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SwipeCell
        let country = countries[indexPath.row]
        cell.nameLabel.text = country.name
        if let currency = country.currencies![0].name {
            cell.currencyLabel.text = currency
        }
        if let language = country.languages![0].name {
            cell.languageLabel.text = language
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    private func requestData() {
        guard let url = URL(string: urlRequest) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            guard let data = data else { return }
            do {
                if error != nil {
                    print(error!.localizedDescription)
                }
                let countriesList = try JSONDecoder().decode([Country].self, from: data)
                //Get back to the main queue
                DispatchQueue.main.async {
                    self.countries = countriesList
                    self.tableView.reloadData()
                }
            }  catch let jsonError {
                print(jsonError)
            }
            }.resume()
    }
    
    func setInterface() {
        self.navigationItem.title = navigationTitle
    }
}

extension ListController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default,
                                                title: "",
                                                handler: { (action, indexPath) in
                                                    self.countries.remove(at: indexPath.row)
                                                    tableView.beginUpdates()
                                                    tableView.deleteRows(at: [indexPath], with: .fade)
                                                    tableView.endUpdates()
        })
        deleteAction.backgroundColor = UIColor.purple
        return [deleteAction]
    }
}
