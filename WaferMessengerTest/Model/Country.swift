//
//  Country.swift
//  WaferMessengerTest
//
//  Created by Augusto Cunha Lima on 25/07/2018.
//  Copyright © 2018 Augusto Cunha Lima. All rights reserved.
//

import UIKit

struct Country: Codable {
    let name: String?
    let languages: [Language]?
    let currencies: [Currencie]?
}

struct Language: Codable {
    let nativeName: String?
    let iso639_2: String?
    let name: String?
    let iso639_1: String?
}

struct Currencie: Codable {
    let name: String?
    let symbol: String?
    let code: String?
}

//
//- key : "languages"
//▿ value : 3 elements
//▿ 0 : 4 elements
//▿ 0 : 2 elements
//- key : nativeName
//- value : پښتو
//▿ 1 : 2 elements
//- key : iso639_2
//- value : pus
//▿ 2 : 2 elements
//- key : name
//- value : Pashto
//▿ 3 : 2 elements
//- key : iso639_1
//- value : ps
//
//▿ 1 : 4 elements
//▿ 0 : 2 elements
//- key : nativeName
//- value : Oʻzbek
//▿ 1 : 2 elements
//- key : iso639_2
//- value : uzb
//▿ 2 : 2 elements
//- key : name
//- value : Uzbek
//▿ 3 : 2 elements
//- key : iso639_1
//- value : uz
//▿ 2 : 4 elements
//▿ 0 : 2 elements
//
//- key : nativeName
//- value : Türkmen
//▿ 1 : 2 elements
//- key : iso639_2
//- value : tuk
//▿ 2 : 2 elements
//- key : name
//- value : Turkmen
//▿ 3 : 2 elements
//- key : iso639_1
//- value : tk
//▿ 12 : 2 elements
//
//- key : "currencies"
//▿ value : 1 element
//▿ 0 : 3 elements
//▿ 0 : 2 elements
//- key : name
//- value : Afghan afghani
//▿ 1 : 2 elements
//- key : symbol
//- value : ؋
//▿ 2 : 2 elements
//- key : code
//- value : AFN
//▿ 21 : 2 elements
